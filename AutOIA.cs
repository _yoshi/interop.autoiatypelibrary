﻿using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
  [CoClass(typeof (AutOIAClass))]
  [Guid("7CCCE800-84F0-11D0-911C-0004AC3617E1")]
  [ComImport]
  public interface AutOIA : IAutOIA, IOIAEvent_Event { }
}

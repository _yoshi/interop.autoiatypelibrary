﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
    [TypeLibType(2)]
    [Guid("7CCCE801-84F0-11D0-911C-0004AC3617E1")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComSourceInterfaces("AutOIATypeLibrary.IOIAEvent")]
    [ComImport]
    public class AutOIAClass : IAutOIA, AutOIA, IOIAEvent_Event
    {
        [DispId(1610743808)]
        public virtual extern bool Alphanumeric { [DispId(1610743808), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743809)]
        public virtual extern bool APL { [DispId(1610743809), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743810)]
        public virtual extern bool Katakana { [DispId(1610743810), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743811)]
        public virtual extern bool Hiragana { [DispId(1610743811), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743812)]
        public virtual extern bool DBCS { [DispId(1610743812), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743813)]
        public virtual extern bool UpperShift { [DispId(1610743813), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743814)]
        public virtual extern bool NumLock { [DispId(1610743814), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743815)]
        public virtual extern bool Numeric { [DispId(1610743815), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743816)]
        public virtual extern bool CapsLock { [DispId(1610743816), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743817)]
        public virtual extern bool InsertMode { [DispId(1610743817), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743818)]
        public virtual extern bool CommErrorReminder { [DispId(1610743818), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743819)]
        public virtual extern bool MessageWaiting { [DispId(1610743819), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743820)]
        public virtual extern InhibitReason InputInhibited { [DispId(1610743820), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743821)]
        public virtual extern string Name { [DispId(1610743821), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

        [DispId(1610743822)]
        public virtual extern int Handle { [DispId(1610743822), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743823)]
        public virtual extern string ConnType { [DispId(1610743823), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

        [DispId(1610743824)]
        public virtual extern int CodePage { [DispId(1610743824), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743825)]
        public virtual extern bool Started { [DispId(1610743825), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743826)]
        public virtual extern bool CommStarted { [DispId(1610743826), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743827)]
        public virtual extern bool APIEnabled { [DispId(1610743827), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743828)]
        public virtual extern bool Ready { [DispId(1610743828), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

        [DispId(1610743829)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void SetConnectionByName([MarshalAs(UnmanagedType.BStr), In] string Name);

        [DispId(1610743830)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void SetConnectionByHandle([In] int Handle);

        [DispId(1610743831)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void StartCommunication();

        [DispId(1610743832)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void StopCommunication();

        [DispId(1610743833)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern bool WaitForInputReady([MarshalAs(UnmanagedType.Struct), In, Optional] object timeout);

        [DispId(1610743834)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern bool WaitForSystemAvailable([MarshalAs(UnmanagedType.Struct), In, Optional] object timeout);

        [DispId(1610743835)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern bool WaitForAppAvailable([MarshalAs(UnmanagedType.Struct), In, Optional] object timeout);

        [DispId(1610743836)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern bool WaitForTransition([MarshalAs(UnmanagedType.Struct), In, Optional] object nIndex, [MarshalAs(UnmanagedType.Struct), In, Optional] object timeout);

        [DispId(1610743837)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void CancelWaits();

        [DispId(1610743838)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void RegisterCommEvent();

        [DispId(1610743839)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void UnregisterCommEvent();

        [DispId(1610743840)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void RegisterOIAEvent();

        [DispId(1610743841)]
        [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        public virtual extern void UnregisterOIAEvent();

        public virtual extern event IOIAEvent_NotifyCommEventEventHandler NotifyCommEvent;

        public virtual extern event IOIAEvent_NotifyCommErrorEventHandler NotifyCommError;

        public virtual extern event IOIAEvent_NotifyCommStopEventHandler NotifyCommStop;

        public virtual extern event IOIAEvent_NotifyOIAEventEventHandler NotifyOIAEvent;

        public virtual extern event IOIAEvent_NotifyOIAErrorEventHandler NotifyOIAError;

        public virtual extern event IOIAEvent_NotifyOIAStopEventHandler NotifyOIAStop;
    }
}

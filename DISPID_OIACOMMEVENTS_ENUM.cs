﻿namespace AutOIATypeLibrary
{
    public enum DISPID_OIACOMMEVENTS_ENUM
    {
        DISPID_OIAEVENT = 1,
        DISPID_OIAEVENTERROR = 2,
        DISPID_OIAEVENTSTOP = 3,
        DISPID_COMMEVENT = 4,
        DISPID_COMMEVENTERROR = 5,
        DISPID_COMMEVENTSTOP = 6,
    }
}

﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
    [Guid("4359F0D0-013F-11D2-B007-0004AC61142E")]
    [TypeLibType(4112)]
    [ComImport]
    public interface IOIAEvent
    {
        [DispId(1)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyOIAEvent();

        [DispId(2)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyOIAError();

        [DispId(3)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyOIAStop([In] ref int Reason);

        [DispId(4)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyCommEvent([In] bool Connected);

        [DispId(5)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyCommError();

        [DispId(6)]
        [MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
        void NotifyCommStop([In] ref int Reason);
    }
}
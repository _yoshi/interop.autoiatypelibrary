﻿using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
    [ComVisible(false)]
    [ComEventInterface(typeof(IOIAEvent), typeof(IOIAEvent_EventProvider))]
    [TypeLibType(16)]
    public interface IOIAEvent_Event
    {
        event IOIAEvent_NotifyCommEventEventHandler NotifyCommEvent;

        event IOIAEvent_NotifyCommErrorEventHandler NotifyCommError;

        event IOIAEvent_NotifyCommStopEventHandler NotifyCommStop;

        event IOIAEvent_NotifyOIAEventEventHandler NotifyOIAEvent;

        event IOIAEvent_NotifyOIAErrorEventHandler NotifyOIAError;

        event IOIAEvent_NotifyOIAStopEventHandler NotifyOIAStop;
    }
}

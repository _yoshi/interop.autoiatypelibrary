﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;

namespace AutOIATypeLibrary
{
    public delegate void IOIAEvent_NotifyCommErrorEventHandler();
    public delegate void IOIAEvent_NotifyCommEventEventHandler([In] bool Connected);
    public delegate void IOIAEvent_NotifyCommStopEventHandler([In] ref int Reason);
    public delegate void IOIAEvent_NotifyOIAErrorEventHandler();
    public delegate void IOIAEvent_NotifyOIAEventEventHandler();
    public delegate void IOIAEvent_NotifyOIAStopEventHandler([In] ref int Reason);

    internal sealed class IOIAEvent_EventProvider : IOIAEvent_Event, IDisposable
    {
        private IConnectionPointContainer m_ConnectionPointContainer;
        private ArrayList m_aEventSinkHelpers;
        private IConnectionPoint m_ConnectionPoint;

        private void Init()
        {
            IConnectionPoint ppCP = null;
            Guid riid = new Guid(new byte[16] { 208, 240, 89, 67, 63, 1, 210, 17, 176, 7, 0, 4, 172, 97, 20, 46 });
            this.m_ConnectionPointContainer.FindConnectionPoint(ref riid, out ppCP);
            this.m_ConnectionPoint = ppCP;
            this.m_aEventSinkHelpers = new ArrayList();
        }

        public IOIAEvent_EventProvider(object autOIAObject)
        {
            this.m_ConnectionPointContainer = (IConnectionPointContainer)autOIAObject;
        }

        event IOIAEvent_NotifyCommEventEventHandler IOIAEvent_Event.NotifyCommEvent
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyCommEventDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for (int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyCommEventDelegate != null && aEventSinkHelper.m_NotifyCommEventDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break; 
                        }
                    }
                }
                finally
                {
                    Monitor.Exit((object)this);
                }
            }
        }

        event IOIAEvent_NotifyCommErrorEventHandler IOIAEvent_Event.NotifyCommError
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyCommErrorDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for (int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyCommErrorDelegate != null && aEventSinkHelper.m_NotifyCommErrorDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break;
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
        }

        event IOIAEvent_NotifyCommStopEventHandler IOIAEvent_Event.NotifyCommStop
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyCommStopDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for (int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyCommStopDelegate != null && aEventSinkHelper.m_NotifyCommStopDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break;
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
        }

        event IOIAEvent_NotifyOIAEventEventHandler IOIAEvent_Event.NotifyOIAEvent
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyOIAEventDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for (int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyOIAEventDelegate != null && aEventSinkHelper.m_NotifyOIAEventDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break;
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
        }

        event IOIAEvent_NotifyOIAErrorEventHandler IOIAEvent_Event.NotifyOIAError
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyOIAErrorDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for (int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyOIAErrorDelegate != null && aEventSinkHelper.m_NotifyOIAErrorDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break;
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
        }

        event IOIAEvent_NotifyOIAStopEventHandler IOIAEvent_Event.NotifyOIAStop
        {
            add
            {
                Monitor.Enter(this);
                try
                {
                    if (this.m_ConnectionPoint == null)
                        this.Init();
                    IOIAEvent_SinkHelper ioiaEventSinkHelper = new IOIAEvent_SinkHelper();
                    int pdwCookie = 0;
                    this.m_ConnectionPoint.Advise(ioiaEventSinkHelper, out pdwCookie);
                    ioiaEventSinkHelper.m_dwCookie = pdwCookie;
                    ioiaEventSinkHelper.m_NotifyOIAStopDelegate = value;
                    this.m_aEventSinkHelpers.Add(ioiaEventSinkHelper);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }

            remove
            {
                Monitor.Enter(this);
                try
                {
                    int count = this.m_aEventSinkHelpers.Count;

                    for(int i = 0; i < count; i++)
                    {
                        IOIAEvent_SinkHelper aEventSinkHelper = (IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i];
                        if (aEventSinkHelper.m_NotifyOIAStopDelegate != null && aEventSinkHelper.m_NotifyOIAStopDelegate.Equals(value))
                        {
                            this.m_aEventSinkHelpers.RemoveAt(i);
                            this.m_ConnectionPoint.Unadvise(aEventSinkHelper.m_dwCookie);
                            if (count <= 1)
                            {
                                Marshal.ReleaseComObject(this.m_ConnectionPoint);
                                this.m_ConnectionPoint = null;
                                this.m_aEventSinkHelpers = null;
                            }
                            break;
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
        }

        bool disposed = false;
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            Monitor.Enter(this);
            try
            {
                if (this.m_ConnectionPoint == null)
                    return;
                int count = this.m_aEventSinkHelpers.Count;

                for (int i = 0; i < count; i++)
                {
                    this.m_ConnectionPoint.Unadvise(((IOIAEvent_SinkHelper)this.m_aEventSinkHelpers[i]).m_dwCookie);
                }
                Marshal.ReleaseComObject(this.m_ConnectionPoint);
            }
            catch { }
            finally
            {
                Monitor.Exit(this);
            }

            disposed = true;
        }

        ~IOIAEvent_EventProvider()
        {
            Dispose(false);
        }
    }
}

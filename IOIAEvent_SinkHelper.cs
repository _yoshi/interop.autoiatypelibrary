﻿using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
    [ClassInterface(ClassInterfaceType.None)]
    public sealed class IOIAEvent_SinkHelper : IOIAEvent
    {
        public IOIAEvent_NotifyOIAStopEventHandler m_NotifyOIAStopDelegate;
        public IOIAEvent_NotifyOIAErrorEventHandler m_NotifyOIAErrorDelegate;
        public IOIAEvent_NotifyOIAEventEventHandler m_NotifyOIAEventDelegate;
        public IOIAEvent_NotifyCommStopEventHandler m_NotifyCommStopDelegate;
        public IOIAEvent_NotifyCommErrorEventHandler m_NotifyCommErrorDelegate;
        public IOIAEvent_NotifyCommEventEventHandler m_NotifyCommEventDelegate;
        public int m_dwCookie;

        public void NotifyOIAStop(ref int Reason)
        {
            if (this.m_NotifyOIAStopDelegate == null)
                return;
            this.m_NotifyOIAStopDelegate(ref Reason);
        }

        public void NotifyOIAError()
        {
            if (this.m_NotifyOIAErrorDelegate == null)
                return;
            this.m_NotifyOIAErrorDelegate();
        }

        public void NotifyOIAEvent()
        {
            if (this.m_NotifyOIAEventDelegate == null)
                return;
            this.m_NotifyOIAEventDelegate();
        }

        public void NotifyCommStop(ref int Reason)
        {
            if (this.m_NotifyCommStopDelegate == null)
                return;
            this.m_NotifyCommStopDelegate(ref Reason);
        }

        public void NotifyCommError()
        {
            if (this.m_NotifyCommErrorDelegate == null)
                return;
            this.m_NotifyCommErrorDelegate();
        }

        public void NotifyCommEvent(bool Connected)
        {
            if (this.m_NotifyCommEventDelegate == null)
                return;
            this.m_NotifyCommEventDelegate(Connected);
        }

        internal IOIAEvent_SinkHelper()
        {
            this.m_dwCookie = 0;
            this.m_NotifyOIAStopDelegate = null;
            this.m_NotifyOIAErrorDelegate = null;
            this.m_NotifyOIAEventDelegate = null;
            this.m_NotifyCommStopDelegate = null;
            this.m_NotifyCommErrorDelegate = null;
            this.m_NotifyCommEventDelegate = null;
        }
    }
}

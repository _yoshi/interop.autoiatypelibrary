﻿using System.Runtime.InteropServices;

namespace AutOIATypeLibrary
{
    [Guid("61AF3D90-2BCC-11D2-A3D9-000629B05A9E")]
    public enum InhibitReason
    {
        pcNotInhibited,
        pcSystemWait,
        pcCommCheck,
        pcProgCheck,
        pcMachCheck,
        pcOtherInhibit,
    }
}
